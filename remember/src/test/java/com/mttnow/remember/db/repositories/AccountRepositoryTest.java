package com.mttnow.remember.db.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mttnow.remember.core.services.exeptions.TaskDoesNotExistExeption;
import com.mttnow.remember.core.services.exeptions.UserDoesNotExist;
import com.mttnow.remember.db.entities.Task;
import com.mttnow.remember.db.entities.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/application-config.xml")
public class AccountRepositoryTest {
	private static final long USER_ID = 1L;

	@Autowired
	private AccountRepository repository;

	private Task task;
	private List<Task> taskList;

	@Before
	@Transactional
	@Rollback(false)
	public void setup() throws UserDoesNotExist {
		taskList = new ArrayList<>();
		
		task = new Task();
		task.setCreationDate(new Date());
		task.setDescription("description");
		
		Task task2 = new Task();
		task2.setCreationDate(new Date());
		task2.setDescription("description2");
		
		task = repository.createTask(USER_ID, task);
		repository.createTask(USER_ID, task2);
		
		taskList.add(task);
		taskList.add(task2);
	}

	@Test
	@Transactional
	public void testFindTask() {
		Long taskId = task.getId();
		Task task = repository.findTask(taskId);
		assertNotNull(task);
		assertEquals("description", task.getDescription());
		assertFalse(task.isDone());
	}
	
	@Test
	@Transactional
	public void testFindAllTasks() {
		List<Task> allTasks = repository.findAllTasks(USER_ID);
		assertNotNull(allTasks);
		assertEquals(2, allTasks.size());
		assertEquals("description", allTasks.get(0).getDescription());
		assertEquals("description2", allTasks.get(1).getDescription());
	}
	
	@Test
	@Transactional
	public void testUpdateTask() throws TaskDoesNotExistExeption {
		task.setDone(true);
		task.setDescription("new description");
		repository.updateTask(task.getId(), task);
		
		Long taskId = task.getId();
		Task task = repository.findTask(taskId);
		
		assertNotNull(task);
		assertEquals("new description", task.getDescription());
		assertTrue(task.isDone());
	}
	
	@Test
	@Transactional
	public void testDeleteTask() {
		Long taskId = task.getId();
		repository.deleteTask(taskId);
		
		Task task = repository.findTask(taskId);
		
		assertNull(task);
	}
	
	@Test
	@Transactional
	public void testFindUserById() {
		User user = repository.findUserById(USER_ID);
		
		assertNotNull(user);
		assertEquals("test", user.getUsername());
	}
	
	@Test
	@Transactional
	public void testFindUserByUsername() {
		User user = repository.findUserByUsername("test");
		
		assertNotNull(user);
		assertTrue(USER_ID == user.getId());
	}
}
