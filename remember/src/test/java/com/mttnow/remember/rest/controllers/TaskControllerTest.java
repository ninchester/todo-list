package com.mttnow.remember.rest.controllers;

import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.mttnow.remember.core.services.TaskService;
import com.mttnow.remember.core.services.exeptions.TaskDoesNotExistExeption;
import com.mttnow.remember.core.services.exeptions.UserDoesNotExist;
import com.mttnow.remember.db.entities.Task;

public class TaskControllerTest {
	@InjectMocks
	private TaskController taskController;

	@Mock
	private TaskService taskService;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		mockMvc = MockMvcBuilders.standaloneSetup(taskController).build();
	}

	@Test
	public void createTask() throws Exception, UserDoesNotExist {
		Task task = new Task();
		task.setDescription("Task Description");

		Date creationDate = getDateByString("10.06.2015");

		task.setCreationDate(creationDate);
		task.setId(1L);

		when(taskService.createTask(eq(1L), any(Task.class))).thenReturn(task);

		mockMvc.perform(
				post("/rest/1/tasks").content("{\"description\":\"Task Description\",\"creationDate\":\"2015-06-10\"}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.description", is(task.getDescription())))
				.andExpect(jsonPath("$.creationDate", is("10.06.2015 12:00:00 AM"))).andExpect(jsonPath("$.done", is(false)))
				.andExpect(jsonPath("$.links[*].href", hasItem(endsWith("rest/tasks/1"))))
				.andExpect(header().string("Location", endsWith("rest/tasks/1"))).andExpect(status().isCreated());
	}

	@Test
	public void updateTask() throws Exception, TaskDoesNotExistExeption {
		Task updatedTask = new Task();
		updatedTask.setId(1L);
		updatedTask.setDescription("Task Description Modified");
		updatedTask.setCreationDate(getDateByString("22.06.2015"));
		updatedTask.setDone(true);

		when(taskService.updateTask(eq(1L), any(Task.class))).thenReturn(updatedTask);

		mockMvc.perform(put("/rest/tasks/1")
				.content("{\"description\":\"Task Description Modified\",\"creationDate\":\"2015-06-22\",\"done\":\"true\"}")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.description", is(updatedTask.getDescription())))
				.andExpect(jsonPath("$.creationDate", is("22.06.2015 12:00:00 AM"))).andExpect(jsonPath("$.done", is(true)))
				.andExpect(jsonPath("$.links[*].href", hasItem(endsWith("/tasks/1"))))
				.andExpect(status().isOk());
	}
	
	@Test
	public void deleteTask() throws Exception {
		Task deletedTask = new Task();
		deletedTask.setId(1L);
		deletedTask.setDescription("Task Description");
		deletedTask.setCreationDate(getDateByString("22.06.2015"));
		deletedTask.setDone(true);

		when(taskService.deleteTask(1L)).thenReturn(deletedTask);

		mockMvc.perform(delete("/rest/tasks/1")
				.content("{\"description\":\"Task Description\",\"creationDate\":\"2015-06-22\",\"done\":\"true\"}")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.description", is(deletedTask.getDescription())))
				.andExpect(jsonPath("$.creationDate", is("22.06.2015 12:00:00 AM"))).andExpect(jsonPath("$.done", is(true)))
				.andExpect(jsonPath("$.links[*].href", hasItem(endsWith("/tasks/1"))))
				.andExpect(status().isOk());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findAllTasks() throws Exception {
		List<Task> taskList = new ArrayList<Task>();

		Task task1 = new Task();
		task1.setId(1L);
		task1.setDescription("description of task1");
		task1.setCreationDate(getDateByString("11.06.2015"));
		taskList.add(task1);

		Task task2 = new Task();
		task2.setId(2L);
		task2.setDescription("description of task2");
		task2.setCreationDate(getDateByString("12.06.2015"));
		task2.setDone(true);

		taskList.add(task2);

		when(taskService.findAllTasks(1L)).thenReturn(taskList);

		mockMvc.perform(get("/rest/1/tasks"))
				.andExpect(jsonPath("$.tasks[*].description",
						hasItems(endsWith("description of task1"), endsWith("description of task2"))))
				.andExpect(status().isOk());
	}

	@Test
	public void findTask() throws Exception {
		Task task = new Task();
		task.setId(1L);
		task.setDescription("description of task1");
		task.setCreationDate(getDateByString("11.06.2015"));

		when(taskService.findTask(1L)).thenReturn(task);

		mockMvc.perform(get("/rest/tasks/1")).andExpect(jsonPath("$.description", is(task.getDescription())))
				.andExpect(jsonPath("$.creationDate", is("11.06.2015 12:00:00 AM"))).andExpect(jsonPath("$.done", is(false)))
				.andExpect(status().isOk());
	}

	private Date getDateByString(String dateStr) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date creationDate = dateFormat.parse(dateStr);
		return creationDate;
	}

}
