package com.mttnow.remember.testsuits;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.mttnow.remember.db.repositories.AccountRepositoryTest;
import com.mttnow.remember.rest.controllers.TaskControllerTest;

@RunWith(Suite.class)
@SuiteClasses({AccountRepositoryTest.class, TaskControllerTest.class})
public class AllTests {

}
