angular.module('ngBoilerplate.tasks', ['ui.router', 'ngResource'])
    .config(function ($stateProvider) {
        $stateProvider.state('tasks', {
            url: '/tasks',
            views: {
                'main': {
                    templateUrl: 'tasks/manage-tasks.tpl.html',
                    controller: 'ManageTasksCtrl'
                }
            },
            data: {pageTitle: "Tasks"}
        });
    })
    .factory('tasksService', function ($resource, $q, $http) {
        var service = {};
        service.createTask = function (accountId, tasksData, success, failure) {
            var Tasks = $resource("/remember/rest/:paramAccountId/tasks");
            return Tasks.save({paramAccountId: accountId}, tasksData).$promise
                .then(function (data) {
                    // success handler
                    success();
                }, function (error) {
                    // error handler
                    failure();
                });
        };

        service.getTasksForAccount = function (accountId) {
            var deferred = $q.defer();
            var Task = $resource("/remember/rest/:paramAccountId/tasks/?rnd=" + new Date().getTime());
            return Task.get({paramAccountId: accountId}).$promise;
        };

        service.updateTask = function (tasksData, success, failure) {
            var restUrl = tasksData.links[0].href;
            var task = {
                done: !tasksData.done
            };

            return $http.put(restUrl, task);
        };

        service.deleteTask = function (tasksData, success, failure) {
            var urlResource = tasksData.links[0].href;
            var Tasks = $resource(urlResource);
            return Tasks.remove().$promise
                .then(function (data) {
                    // success handler
                    success();
                }, function (error) {
                    // error handler
                    failure();
                });
        };

        return service;
    })
    .controller('ManageTasksCtrl', function ($scope, tasksService, $state, sessionService) {

        $scope.isLoggedIn = sessionService.isLoggedIn;

        var currentUser = sessionService.getCurrentUser();
        $scope.currentUsername = currentUser.username;

        init(function(){});

        function init(success) {
            tasksService.getTasksForAccount(currentUser.userId)
                .then(function (data) {
                    $scope.tasks = data.tasks;
                    success();
                });
        }

        $scope.logout = function() {
            sessionService.logout();
            $state.go("login", {reload: true});
        };

        $scope.createTask = function (description) {
            if (description) {
                tasksService.createTask(currentUser.userId, {description: description}, function () {
                    init(function() {
                        $state.go("tasks", {reload: true});
                    });
                }, function () {
                    alert('Could not create a task. Please, try again..');
                });
            }
        };

        $scope.updateTask = function (task) {
            tasksService.updateTask(task, function () {
                init(function() {
                    $state.go("tasks", {reload: true});
                });
            }, function () {
                alert('Could not update a task. Please try again..');
            });
        };

        $scope.deleteTask = function (task) {
            tasksService.deleteTask(task, function () {
                init(function() {
                    $state.go("tasks", {reload: true});
                });
            }, function () {
                alert('Could not delete a task. Please try again..');
            });

        };
    });