angular.module('ngBoilerplate.account', ['ui.router'])
    .config(function ($stateProvider) {
        $stateProvider.state('login', {
            url: '/login',
            views: {
                'main': {
                    templateUrl: 'account/login.tpl.html',
                    controller: 'LoginCtrl'
                }
            },
            data: {pageTitle: "Login"}
        });
    })
    .factory('sessionService', function ($resource, $q) {
        var session = {};
        var user;
        session.login = function (data, success, failure) {
            var deferred = $q.defer();
            var User = $resource("/remember/rest/users/?username=:paramUsername");
            return User.get({paramUsername: data.username}).$promise.then(function (result) {
                if (result) {
                    user = result;
                    success();
                } else {
                    failure();
                }
            });
        };
        session.logout = function () {
            user = null;
        };
        session.isLoggedIn = function () {
            return user;
        };

        session.getCurrentUser = function() {
            return user;
        };
        return session;
    })
    .controller("LoginCtrl", function ($scope, sessionService, $state) {
        $scope.login = function () {
           var result = sessionService.login($scope.account,
               function() {
                    $state.go("tasks");
            }, function() {
                alert('wrong cretentials..');
            });
        };

    });
