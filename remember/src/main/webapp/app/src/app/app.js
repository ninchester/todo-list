angular.module( 'ngBoilerplate', [
  'templates-app',
  'templates-common',
  'ngBoilerplate.account',
  'ngBoilerplate.tasks',
  'ui.router',
  'ngResource'
])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise( '/tasks' );
})

.run( function run () {
})

.controller( 'AppCtrl', function AppCtrl ( $scope, $location, sessionService ) {
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if (!sessionService.isLoggedIn()) {
      event.preventDefault();
      $location.path('/login');
    }

    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = 'ToDo List Application' + toState.data.pageTitle;
    }
  });
})

;

