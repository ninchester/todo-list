angular.module('templates-app', ['account/login.tpl.html', 'account/register.tpl.html', 'tasks/manage-tasks.tpl.html']);

angular.module("account/login.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("account/login.tpl.html",
    "<div class=\"row jumbotron\">\n" +
    "    <h1>ToDo List</h1>\n" +
    "    <p class=\"lead\">\n" +
    "        Welcome to ToDo List Application!\n" +
    "        In order to view your todo list you should login.\n" +
    "    </p>\n" +
    "\n" +
    "    <form ng-controller=\"LoginCtrl\" ng-submit=\"login()\" name=\"loginForm\" class=\"form-signin\">\n" +
    "        <h2 class=\"form-signin-heading\">\n" +
    "            Login\n" +
    "        </h2>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label>Username:</label>\n" +
    "            <input type=\"text\" name=\"username\" ng-model=\"account.username\" class=\"form-control\"/>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label>Password:</label>\n" +
    "            <input type=\"password\" name=\"password\" ng-model=\"account.password\" class=\"form-control\"/>\n" +
    "        </div>\n" +
    "        <button class=\"btn btn-success\" type=\"submit\">Login</button>\n" +
    "    </form>\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("account/register.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("account/register.tpl.html",
    "<div class=\"row\">\n" +
    "  <h1 class=\"page-header\">\n" +
    "      Register\n" +
    "  </h1>\n" +
    "  <form ng-submit=\"register()\">\n" +
    "      <div class=\"form-group\">\n" +
    "          <label>Username:</label>\n" +
    "          <input type=\"text\" ng-model=\"account.name\" class=\"form-control\" />\n" +
    "      </div>\n" +
    "      <div class=\"form-group\">\n" +
    "          <label>Password:</label>\n" +
    "          <input type=\"password\" ng-model=\"account.password\" class=\"form-control\" />\n" +
    "      </div>\n" +
    "      <button class=\"btn btn-success\" type=\"submit\">Register</button>\n" +
    "  </form>\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("tasks/manage-tasks.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("tasks/manage-tasks.tpl.html",
    "<div ng-show=\"isLoggedIn()\" class=\"row\">\n" +
    "    <p class=\"col-md-11\">\n" +
    "        Welcome, {{currentUsername}}!\n" +
    "    </p>\n" +
    "    <a ng-click=\"logout()\" class=\"btn btn-small btn-default col-md-1\">\n" +
    "        <i class=\"fa fa-sign-out\"></i>\n" +
    "        Logout\n" +
    "    </a>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"jumbotron\">\n" +
    "    <h1>ToDo List</h1>\n" +
    "\n" +
    "    <p class=\"lead\" ng-hide=\"isLoggedIn()\">\n" +
    "        Welcome to ToDo List Application!\n" +
    "        In order to view your todo list you should login.\n" +
    "    </p>\n" +
    "\n" +
    "    <div class=\"btn-group\" ng-hide=\"isLoggedIn()\">\n" +
    "        <a ui-sref=\"login\" class=\"btn btn-large btn-default\">\n" +
    "            <i class=\"fa fa-sign-in\"></i>\n" +
    "            Login\n" +
    "        </a>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div ng-show=\"isLoggedIn()\">\n" +
    "    <p class=\"lead\">\n" +
    "        Your current todo list:\n" +
    "    </p>\n" +
    "\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"input-group component-group\">\n" +
    "            <input type=\"text\" class=\"form-control\" ng-model=\"newTaskDescription\">\n" +
    "      <span class=\"input-group-btn\">\n" +
    "        <button class=\"btn btn-default\" type=\"button\" ng-click=\"createTask(newTaskDescription)\">Create New Task</button>\n" +
    "      </span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <table class=\"table table-striped\">\n" +
    "            <th>Task Description</th>\n" +
    "            <th colspan=\"2\">Action</th>\n" +
    "            <tr ng-show=\"!tasks.length\">\n" +
    "                <td colspan=\"3\">Currently you do not have any tasks to do - just relax ;)</td>\n" +
    "            </tr>\n" +
    "            <tr ng-show=\"tasks.length\" ng-repeat=\"task in tasks\">\n" +
    "                <td>\n" +
    "                    <dl>\n" +
    "                        <dt>{{task.description}}</dt>\n" +
    "                        <dd>Creation date: {{task.creationDate}}</dd>\n" +
    "                    </dl>\n" +
    "                </td>\n" +
    "                <td>\n" +
    "                    <input id=\"isDone\" type=\"checkbox\" ng-checked=\"task.done\" ng-click=\"updateTask(task)\">\n" +
    "                </td>\n" +
    "                <td>\n" +
    "                    <a ui-sref=\"tasks\" class=\"btn btn-large btn-default\" ng-click=\"deleteTask(task)\">\n" +
    "                        Delete\n" +
    "                    </a>\n" +
    "                </td>\n" +
    "            </tr>\n" +
    "        </table>\n" +
    "    </div>\n" +
    "</div>");
}]);
