DROP TABLE User IF EXISTS;

CREATE TABLE User (
  id BIGINT NOT NULL PRIMARY KEY,
  username VARCHAR(50), 
  password VARCHAR(50)
);

INSERT INTO User (id, username, password) VALUES (1, 'test', 'abc123');
INSERT INTO User (id, username, password) VALUES (2, 'admin', 'admin');