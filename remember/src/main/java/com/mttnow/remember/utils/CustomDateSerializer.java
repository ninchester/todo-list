package com.mttnow.remember.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * Serializing a {@link Date} in a custom format.
 * 
 * @author Nina Nikolova
 *
 */
public class CustomDateSerializer extends JsonSerializer<Date> {

	@Override
	public void serialize(Date date, JsonGenerator gen, SerializerProvider serProvider)
			throws IOException, JsonProcessingException {

		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss a");
		String formattedDate = formatter.format(date);

		gen.writeString(formattedDate);
	}
}
