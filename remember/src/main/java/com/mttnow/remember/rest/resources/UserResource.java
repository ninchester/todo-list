package com.mttnow.remember.rest.resources;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mttnow.remember.db.entities.User;

/**
 * This class represents a model of object user used in REST communication.
 * 
 * @author Nina Nikolova
 *
 */
public class UserResource extends ResourceSupport {
	private String userId;
	
	private String username;

	private String password;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	public User toUser() {
		User user = new User();
		user.setId((userId != null)? Long.valueOf(userId) : 0L);
		user.setUsername(username);
		user.setPassword(password);
		return user;
	}
}
