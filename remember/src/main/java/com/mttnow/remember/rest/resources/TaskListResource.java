package com.mttnow.remember.rest.resources;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;

/**
 * This class represents a model of list of tasks used in REST communication.
 * 
 * @author Nina Nikolova
 *
 */
public class TaskListResource extends ResourceSupport {

	private List<TaskResource> tasks = new ArrayList<>();

	public List<TaskResource> getTasks() {
		return tasks;
	}

	public void setTasks(List<TaskResource> tasks) {
		this.tasks = tasks;
	}
}
