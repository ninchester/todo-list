package com.mttnow.remember.rest.resources.assemblers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import com.mttnow.remember.db.entities.Task;
import com.mttnow.remember.rest.controllers.TaskController;
import com.mttnow.remember.rest.resources.TaskResource;

/**
 * Class responsible for converting an entity task class to its REST model.
 * 
 * @author Nina Nikolova
 *
 */
public class TaskResourceAsm extends ResourceAssemblerSupport<Task, TaskResource> {

	public TaskResourceAsm() {
		super(TaskController.class, TaskResource.class);
	}

	@Override
	public TaskResource toResource(Task task) {
		TaskResource taskResource = new TaskResource();
		taskResource.setDescription(task.getDescription());
		taskResource.setCreationDate(task.getCreationDate());
		taskResource.setDone(task.isDone());
		taskResource.add(linkTo(methodOn(TaskController.class).getTask(task.getId())).withSelfRel());
		return taskResource;
	}

}
