package com.mttnow.remember.rest.resources.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import com.mttnow.remember.db.entities.User;
import com.mttnow.remember.rest.controllers.UserController;
import com.mttnow.remember.rest.resources.UserResource;

/**
 * Class responsible for converting an entity user class to its REST model.
 * 
 * @author Nina Nikolova
 *
 */
public class UserResourceAsm extends ResourceAssemblerSupport<User, UserResource> {

	public UserResourceAsm() {
		super(UserController.class, UserResource.class);
	}

	@Override
	public UserResource toResource(User user) {
		UserResource userResource = new UserResource();
		userResource.setUserId(String.valueOf(user.getId()));
		userResource.setUsername(user.getUsername());
		userResource.setPassword(user.getPassword());
		userResource.add(linkTo(methodOn(UserController.class).getUserById(user.getId())).withSelfRel());
		return userResource;
	}

}
