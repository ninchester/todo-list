package com.mttnow.remember.rest.controllers;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mttnow.remember.core.services.TaskService;
import com.mttnow.remember.core.services.exeptions.TaskDoesNotExistExeption;
import com.mttnow.remember.core.services.exeptions.UserDoesNotExist;
import com.mttnow.remember.db.entities.Task;
import com.mttnow.remember.rest.resources.TaskListResource;
import com.mttnow.remember.rest.resources.TaskResource;
import com.mttnow.remember.rest.resources.assemblers.TaskResourceAsm;

/**
 * A controller class responsible to serve REST calls related to {@link Task}s.
 * 
 * @author Nina Nikolova
 *
 */
@Controller
@RequestMapping("/rest")
public class TaskController {

	private TaskService taskService;

	@Autowired
	public TaskController(TaskService taskService) {
		this.taskService = taskService;
	}

	@RequestMapping(value = "/tasks/{taskId}", method = RequestMethod.GET)
	public ResponseEntity<TaskResource> getTask(@PathVariable Long taskId) {
		Task foundTask = taskService.findTask(taskId);
		if (foundTask != null) {
			TaskResource taskResource = new TaskResourceAsm().toResource(foundTask);
			return new ResponseEntity<TaskResource>(taskResource, HttpStatus.OK);
		} else {
			return new ResponseEntity<TaskResource>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/{userId}/tasks", method = RequestMethod.GET)
	public ResponseEntity<TaskListResource> getAllTask(@PathVariable Long userId) {
		List<Task> taskList = taskService.findAllTasks(userId);
		TaskListResource taskListResource = new TaskListResource();
		List<TaskResource> taskResourceList = new ArrayList<>();
		for (Task task : taskList) {
			TaskResource taskResource = new TaskResourceAsm().toResource(task);
			taskResourceList.add(taskResource);
		}
		taskListResource.setTasks(taskResourceList);

		return new ResponseEntity<TaskListResource>(taskListResource, HttpStatus.OK);
	}

	@RequestMapping(value = "/{userId}/tasks", method = RequestMethod.POST)
	public ResponseEntity<TaskResource> createTask(@PathVariable Long userId, @RequestBody TaskResource taskResource) {
		try {
			Task task = taskService.createTask(userId, taskResource.toTask());
			taskResource = new TaskResourceAsm().toResource(task);
			HttpHeaders headers = new HttpHeaders();
			headers.setLocation(URI.create(taskResource.getLink("self").getHref()));
			return new ResponseEntity<TaskResource>(taskResource, headers, HttpStatus.CREATED);
		} catch (UserDoesNotExist e) {
			return new ResponseEntity<TaskResource>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/tasks/{taskId}", method = RequestMethod.DELETE)
	public ResponseEntity<TaskResource> removeTask(@PathVariable Long taskId) {
		Task deletedTask = taskService.deleteTask(taskId);
		TaskResource taskResource = null;
		if (deletedTask != null) {
			taskResource = new TaskResourceAsm().toResource(deletedTask);
		}
		return new ResponseEntity<TaskResource>(taskResource, HttpStatus.OK);
	}

	@RequestMapping(value = "/tasks/{taskId}", method = RequestMethod.PUT)
	public ResponseEntity<TaskResource> updateTask(@PathVariable Long taskId, @RequestBody TaskResource taskResource) {
		try {
			Task updatetedTask = taskService.updateTask(taskId, taskResource.toTask());
			TaskResource taskResourceReturned = new TaskResourceAsm().toResource(updatetedTask);
			return new ResponseEntity<TaskResource>(taskResourceReturned, HttpStatus.OK);
		} catch (TaskDoesNotExistExeption e) {
			return new ResponseEntity<TaskResource>(HttpStatus.NOT_FOUND);
		}
	}
}
