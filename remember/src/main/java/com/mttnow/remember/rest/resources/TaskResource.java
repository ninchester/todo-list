package com.mttnow.remember.rest.resources;

import java.util.Date;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mttnow.remember.db.entities.Task;
import com.mttnow.remember.utils.CustomDateSerializer;

/**
 * This class represents a model of object task used in REST communication.
 * 
 * @author Nina Nikolova
 *
 */
public class TaskResource extends ResourceSupport {
	private String description;
	private Date creationDate;
	private boolean isDone;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public boolean isDone() {
		return isDone;
	}

	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}

	public Task toTask() {
		Task task = new Task();
		task.setDescription(description);
		task.setCreationDate(creationDate);
		task.setDone(isDone);
		return task;
	}
}
