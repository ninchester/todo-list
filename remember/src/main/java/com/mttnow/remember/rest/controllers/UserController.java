package com.mttnow.remember.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mttnow.remember.core.services.UserService;
import com.mttnow.remember.db.entities.User;
import com.mttnow.remember.rest.resources.UserResource;
import com.mttnow.remember.rest.resources.assemblers.UserResourceAsm;

/**
 * A controller class responsible to serve REST calls related to {@link User}s.
 * 
 * @author Nina Nikolova
 *
 */
@Controller
@RequestMapping("/rest/users")
public class UserController {
	
	private UserService userService;
	
	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}
	
	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public ResponseEntity<UserResource> getUserById(@PathVariable Long userId) {
		try {
			User foundUser = userService.findUserById(userId);
			if (foundUser != null) {
				UserResource userResource = new UserResourceAsm().toResource(foundUser);
				return new ResponseEntity<UserResource>(userResource, HttpStatus.OK);
			} else {
				return new ResponseEntity<UserResource>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception exception) {
			return new ResponseEntity<UserResource>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<UserResource> getUserByUsername(@RequestParam("username") String username) {
		try {
			User foundUser = userService.findUserByUsername(username);
			if (foundUser != null) {
				UserResource userResource = new UserResourceAsm().toResource(foundUser);
				return new ResponseEntity<UserResource>(userResource, HttpStatus.OK);
			} else {
				return new ResponseEntity<UserResource>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception exception) {
			return new ResponseEntity<UserResource>(HttpStatus.BAD_REQUEST);
		}
	}
}
