package com.mttnow.remember.core.services;

import com.mttnow.remember.db.entities.User;

/**
 * Interface describing the operations for users' management.
 * 
 * @author Nina Nikolova
 *
 */
public interface UserService {
	/**
	 * Fetches a user by id.
	 * 
	 * @param userId
	 *            Identifier of the user.
	 * @return if the user is found and object of type {@link User}, otherwise
	 *         returns <code>null</code>.
	 */
	public User findUserById(Long userId);

	/**
	 * Fetches a user by username.
	 * 
	 * @param username
	 *            Username of the user.
	 * @return if the user is found and object of type {@link User}, otherwise
	 *         returns <code>null</code>.
	 */
	public User findUserByUsername(String username);
}
