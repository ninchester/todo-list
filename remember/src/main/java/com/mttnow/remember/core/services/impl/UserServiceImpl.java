package com.mttnow.remember.core.services.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mttnow.remember.core.services.UserService;
import com.mttnow.remember.db.entities.User;
import com.mttnow.remember.db.repositories.AccountRepository;

/**
 * Concrete implementation of {@link UserService}.
 * 
 * @author Nina Nikolova
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private AccountRepository accountRepo;

	@Override
	public User findUserById(Long userId) {
		return accountRepo.findUserById(userId);
	}

	@Override
	public User findUserByUsername(String username) {
		return accountRepo.findUserByUsername(username);
	}

}
