package com.mttnow.remember.core.services.exeptions;

/**
 * Throwable indicating that a user does not exist.
 * 
 * @author Nina Nikolova
 *
 */
public class UserDoesNotExist extends Throwable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserDoesNotExist() {
		super();
	}

	public UserDoesNotExist(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UserDoesNotExist(String message, Throwable cause) {
		super(message, cause);
	}

	public UserDoesNotExist(String message) {
		super(message);
	}

	public UserDoesNotExist(Throwable cause) {
		super(cause);
	}

}
