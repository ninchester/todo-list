package com.mttnow.remember.core.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mttnow.remember.core.services.TaskService;
import com.mttnow.remember.core.services.exeptions.TaskDoesNotExistExeption;
import com.mttnow.remember.core.services.exeptions.UserDoesNotExist;
import com.mttnow.remember.db.entities.Task;
import com.mttnow.remember.db.repositories.AccountRepository;

/**
 * Concrete implementation of {@link TaskService}.
 * 
 * @author Nina Nikolova
 *
 */
@Service
@Transactional
public class TaskServiceImpl implements TaskService {
	@Autowired
	private AccountRepository accountRepo;

	@Override
	public List<Task> findAllTasks(Long userId) {
		return accountRepo.findAllTasks(userId);
	}

	@Override
	public Task findTask(Long taskId) {
		return accountRepo.findTask(taskId);
	}

	@Override
	public Task createTask(Long userId, Task task) throws UserDoesNotExist {
		return accountRepo.createTask(userId, task);
	}

	@Override
	public Task updateTask(Long taskId, Task task) throws TaskDoesNotExistExeption {
		return accountRepo.updateTask(taskId, task);
	}

	@Override
	public Task deleteTask(Long taskId) {
		return accountRepo.deleteTask(taskId);
	}

}
