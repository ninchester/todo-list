package com.mttnow.remember.core.services.exeptions;

/**
 * Throwable indicating that a task does not exist.
 * 
 * @author Nina Nikolova
 *
 */
public class TaskDoesNotExistExeption extends Throwable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TaskDoesNotExistExeption(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public TaskDoesNotExistExeption(String message, Throwable cause) {
		super(message, cause);
	}

	public TaskDoesNotExistExeption(String message) {
		super(message);
	}

	public TaskDoesNotExistExeption(Throwable cause) {
		super(cause);
	}

}
