package com.mttnow.remember.core.services;

import java.util.List;

import com.mttnow.remember.core.services.exeptions.TaskDoesNotExistExeption;
import com.mttnow.remember.core.services.exeptions.UserDoesNotExist;
import com.mttnow.remember.db.entities.Task;

/**
 * Interface describing the operations for tasks' management.
 * 
 * @author Nina Nikolova
 *
 */
public interface TaskService {

	/**
	 * Finds all task for user.
	 * 
	 * @param userId
	 *            Identifier of the user.
	 * @return if tasks are found returns a collection of {@link Task}s,
	 *         otherwise returns empty list.
	 */
	public List<Task> findAllTasks(Long userId);

	/**
	 * Finds a task by id.
	 * 
	 * @param taskId
	 *            Identifier of the task.
	 * @return if the task is found and object of type {@link Task}, otherwise
	 *         returns <code>null</code>.
	 * 
	 */
	public Task findTask(Long taskId);

	/**
	 * Creates a task.
	 * 
	 * @param userId
	 *            Identifier of the user.
	 * @param task
	 *            Object representing a task to be created.
	 * 
	 * @return the created {@link Task}.
	 * 
	 * @throws UserDoesNotExist
	 *             thrown if the user with userId does not exist
	 */
	public Task createTask(Long userId, Task task) throws UserDoesNotExist; 

	/**
	 * Updates an task.
	 * 
	 * @param taskId
	 *            Identifier of the task.
	 * @param task
	 *            Object representing a task to be created.
	 * @return the updated {@link Task}.
	 * 
	 * @throws TaskDoesNotExistExeption
	 *             thrown if the task does not exist
	 */
	public Task updateTask(Long taskId, Task task) throws TaskDoesNotExistExeption; 

	/**
	 * Deletes a task.
	 * 
	 * @param taskId
	 *            Identifier of the task.
	 * @return the deleted {@link Task}.
	 */
	public Task deleteTask(Long taskId);
}
