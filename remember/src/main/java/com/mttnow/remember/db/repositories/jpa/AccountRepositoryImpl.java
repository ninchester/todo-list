package com.mttnow.remember.db.repositories.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.mttnow.remember.core.services.exeptions.TaskDoesNotExistExeption;
import com.mttnow.remember.core.services.exeptions.UserDoesNotExist;
import com.mttnow.remember.db.entities.Task;
import com.mttnow.remember.db.entities.User;
import com.mttnow.remember.db.repositories.AccountRepository;

/**
 * Concrete implementation of {@link AccountRepository}.
 * 
 * @author Nina Nikolova
 *
 */
@Repository
public class AccountRepositoryImpl implements AccountRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Task> findAllTasks(Long userId) {
		return entityManager.createQuery("from Task where owner_id = " + userId, Task.class).getResultList();
	}

	@Override
	public Task createTask(Long userId, Task task) throws UserDoesNotExist {
		User owner = findUserById(userId);
		if (owner != null) {
			task.setOwner(owner);
			task.setCreationDate(new Date());
			entityManager.persist(task);
			entityManager.flush();
		} else {
			throw new UserDoesNotExist("Task cannot be created for user. User does not exist.");
		}

		return task;
	}

	@Override
	public Task updateTask(Long taskId, Task task) throws TaskDoesNotExistExeption {
		Task taskToUpdate = entityManager.find(Task.class, taskId);
		if (taskToUpdate == null) {
			throw new TaskDoesNotExistExeption(String.format("Task [%s] does not exist.", taskId));
		}
		if ((task.getDescription() != null) && !task.getDescription().isEmpty()) {
			taskToUpdate.setDescription(task.getDescription());
		}
		taskToUpdate.setDone(task.isDone());
		entityManager.persist(taskToUpdate);
		return taskToUpdate;
	}

	@Override
	public Task deleteTask(Long taskId) {
		Task task = findTask(taskId);
		if (task != null) {
			entityManager.remove(task);
		}

		return task;
	}

	@Override
	public Task findTask(Long taskId) {
		return entityManager.find(Task.class, taskId);
	}

	@Override
	public User findUserById(Long userId) {
		return entityManager.find(User.class, userId);
	}

	@Override
	public User findUserByUsername(String username) {
		return entityManager.createQuery("from User where username = '" + username + "'", User.class).getSingleResult();
	}

}
