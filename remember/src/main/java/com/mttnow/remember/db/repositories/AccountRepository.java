package com.mttnow.remember.db.repositories;

import java.util.List;

import com.mttnow.remember.core.services.exeptions.TaskDoesNotExistExeption;
import com.mttnow.remember.core.services.exeptions.UserDoesNotExist;
import com.mttnow.remember.db.entities.Task;
import com.mttnow.remember.db.entities.User;

/**
 * Interface representing a JPA repository for interacting with the objects
 * stored into the database.
 * 
 * @author Nina Nikolova
 *
 */
public interface AccountRepository {

	/**
	 * Fetches a user by id.
	 * 
	 * @param userId
	 *            Identifier of the user.
	 * @return if the user is found and object of type {@link User}, otherwise
	 *         returns <code>null</code>.
	 */
	public User findUserById(Long userId);

	/**
	 * Fetches a user by username.
	 * 
	 * @param username
	 *            Username of the user.
	 * @return if the user is found and object of type {@link User}, otherwise
	 *         returns <code>null</code>.
	 */
	public User findUserByUsername(String username);

	/**
	 * Fetches all task for a particular user.
	 * 
	 * @param userId
	 *            Identifier of the user.
	 * @return if tasks are found returns a collection of {@link Task}s,
	 *         otherwise returns empty list.
	 */
	public List<Task> findAllTasks(Long userId);

	/**
	 * Fetches a task by id.
	 * 
	 * @param taskId
	 *            Identifier of the task.
	 * @return if the task is found and object of type {@link Task}, otherwise
	 *         returns <code>null</code>.
	 *         
	 */
	public Task findTask(Long taskId);

	/**
	 * Creates a task.
	 * 
	 * @param userId
	 *            Identifier of the user.
	 * @param task
	 *            Object representing a task to be created.
	 *            
	 * @return the created {@link Task}.
	 * 
	 * @throws UserDoesNotExist
	 *             thrown if the user with userId does not exist
	 */
	public Task createTask(Long userId, Task task) throws UserDoesNotExist;

	/**
	 * Updates an task.
	 * 
	 * @param taskId
	 *            Identifier of the task.
	 * @param task
	 *            Object representing a task to be created.
	 * @return the updated {@link Task}.
	 * 
	 * @throws TaskDoesNotExistExeption
	 *             thrown if the task does not exist
	 */
	public Task updateTask(Long taskId, Task task) throws TaskDoesNotExistExeption;

	/**
	 * Deletes a task.
	 * 
	 * @param taskId
	 *            Identifier of the task.
	 * @return the deleted {@link Task}.
	 */
	public Task deleteTask(Long taskId);
}
